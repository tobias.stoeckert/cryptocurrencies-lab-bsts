extern crate native_tls;

use native_tls::{Identity, TlsAcceptor, TlsStream};
use std::fs::File;
use std::io::{Read,Write};
use std::net::{TcpListener, TcpStream};
use std::sync::Arc;
use std::thread;
use std::{env};
extern crate rand;
use rand::{Rng};

extern crate time;
use time::PreciseTime;

extern crate curves;
use curves::{Ford};
use curves::f_4141::FSecp256Ord;

extern crate mpecdsa;

extern crate getopts;
use self::getopts::{Options, Matches};

pub fn process_options() -> Option<Matches> {
    let args: Vec<String> = env::args().collect();

    println!("args: {:?}", args);

    let mut opts = Options::new();
    opts.optopt("p", "port", "set port", "PORT");
    opts.optopt("c", "client", "set ip address of server", "IP");
    opts.optopt("m", "message", "The Message that you want to sign", "MESSAGE");

    opts.optflag("h", "help", "print this help menu");
    opts.optflag("b", "bob", "run as bob");
    opts.optflag("", "bench_setup", "benchmark setup");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m }
        Err(f) => { panic!(f.to_string()) }
    };

    if matches.opt_present("h") {
        let program = args[0].clone();
        let brief = format!("Usage: {} [options]", program);
        print!("{}", opts.usage(&brief));
        return Option::None;
    }


    return Option::Some(matches);

}
fn handle_client(stream: &mut TlsStream<TcpStream>, bob : bool) {
    // ...

}
fn main() {
    let mut file = File::open("/home/tobias/mpecdsa_git/src/bin/identity.pfx").unwrap();
    let mut identity = vec![];
    let mut file2 = File::open("/home/tobias/mpecdsa_git/src/bin/identity.pfx").unwrap();
    let mut identity2 = vec![];
    file.read_to_end(&mut identity).unwrap();
    file2.read_to_end(&mut identity2).unwrap();
    let identity = Identity::from_pkcs12(&identity, "test").unwrap();
    let identity2 = Identity::from_pkcs12(&identity2, "test").unwrap();
    let matches = process_options();
    if let None = matches {
        ::std::process::exit(1);
    }
    let matches = matches.unwrap();
    let message = format!("{}",matches.opt_str("m").unwrap_or("defaultmsg".to_owned()));
    let msg = message.as_bytes();
    let mut seeder = rand::os::OsRng::new().unwrap();
    let mut rng = rand::ChaChaRng::new_unseeded();
    rng.set_counter(seeder.gen::<u64>(), seeder.gen::<u64>());
    let port = format!("0.0.0.0:{}",matches.opt_str("p").unwrap_or("12345".to_owned()));
    println!("Waiting for client to connect on {}", port);
    let listener = TcpListener::bind(port).unwrap();
    let acceptor = TlsAcceptor::new(identity).unwrap();
    let acceptor = Arc::new(acceptor);
    let (mut streamrcv, _) = listener.accept().unwrap_or_else(|e| {panic!(e) });
    println!("TcpStream generated");
    let mut stream2 = streamrcv.try_clone().unwrap();
    let mut stream1 = acceptor.accept(streamrcv).unwrap();
    println!("tls stream");

    println!("TcpStream generated");


    let acceptor2 = TlsAcceptor::new(identity2).unwrap();
    let acceptor2 = Arc::new(acceptor2);
    let mut stream2 = acceptor2.accept(stream2).unwrap();
    stream1.write("asdf".as_bytes());
    let mut buf = [0u8];
    stream2.read_exact(&mut buf);
    println!("{:?}",buf);
    println!("connection accepted");
    stream1.write("asdf".as_bytes());
    let mut buf = [0u8];
    stream2.read_exact(&mut buf);
    println!("{:?}",buf);
    println!("connection accepted");

    //let mut stream1;
    //let mut stream2;
    //let mut i = 0;
    /*for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                let acceptor = acceptor.clone();
                let acceptor2 = acceptor2.clone();
                thread::spawn(move || {
                    if(i == 0){
                        stream1 =  acceptor.accept(stream).unwrap();
                    }
                    if(i == 1){
                        stream2 = acceptor2.accept(stream).unwrap();
                    }



                });
            }
            Err(e) => { /* connection failed */ }
        }
        i = i +1;
    }*/
    if matches.opt_present("b") {
        let skb = FSecp256Ord::from_slice(&[0xb75db4463a602ff0,
            0x83b6a76e7fad1ec, 0xa33f33b8e9c84dbd, 0xb94fceb9fff7cfb2]);

        println!("Starting setup as Bob...");
        let bob = mpecdsa::mpecdsa::Bob2P::new(
            &skb, &mut rng, &mut stream1,&mut stream2).unwrap();
        println!("Setup finished");
        println!("The following message will be signed: {:?}", message);
        let sig=  bob.sign(
            &msg, &mut rng, &mut stream1,&mut stream2).unwrap();
        println!("Successful signature creation");
        //println!("{:?}",sig);



    } else {
        let ska = FSecp256Ord::from_slice(&[0xc93d9fa738a8b4b6,
            0xe8dd5f4af65e7462, 0xcbdf97aeca50c5c4, 0x67498f7dcab40d3]);
        println!("Starting setup as Alice...");
        let alice = mpecdsa::mpecdsa::Alice2P::new(
            &ska, &mut rng, &mut stream1,&mut stream2).unwrap();
        println!("Setup finished");
        println!("The following message will be signed: {:?}", message);
        alice.sign(&msg, &mut rng, &mut stream1,&mut stream2).unwrap();
        println!("Successful signature creation");



    }
}