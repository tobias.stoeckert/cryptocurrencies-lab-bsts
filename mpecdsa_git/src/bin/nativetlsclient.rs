extern crate native_tls;
use native_tls::{TlsConnector,Certificate};
use std::io::{Read, Write};
use std::net::TcpStream;
use std::fs::File;
use std::vec::Vec;
use std::clone::Clone;
use std::{env};
extern crate rand;
use rand::{Rng};

extern crate time;
use time::PreciseTime;

extern crate curves;
use curves::{Ford};
use curves::f_4141::FSecp256Ord;

extern crate mpecdsa;

extern crate getopts;
use self::getopts::{Options, Matches};

pub fn process_options() -> Option<Matches> {
    let args: Vec<String> = env::args().collect();

    println!("args: {:?}", args);

    let mut opts = Options::new();
    opts.optopt("p", "port", "set port", "PORT");
    opts.optopt("c", "client", "set ip address of server", "IP");
    opts.optopt("m", "message", "The Message that you want to sign", "MESSAGE");

    opts.optflag("h", "help", "print this help menu");
    opts.optflag("b", "bob", "run as bob");
    opts.optflag("", "bench_setup", "benchmark setup");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m }
        Err(f) => { panic!(f.to_string()) }
    };

    if matches.opt_present("h") {
        let program = args[0].clone();
        let brief = format!("Usage: {} [options]", program);
        print!("{}", opts.usage(&brief));
        return Option::None;
    }


    return Option::Some(matches);

}
fn main() {

    let mut builder = TlsConnector::builder();
    /*let mut file = File::open("/home/tobias/mpecdsa_git/src/bin/rootcert.pem").unwrap();
    let mut p = Vec::new();
    file.read_to_end(&mut p);
    let cert = Certificate::from_pem(&mut p);*/
    /*
    Trying to add root cert, if it doesnt work accept invalid certs
    This is for testing locally only!!!
    */
    /*match cert {
        Ok(cert) => {builder.add_root_certificate(cert);
        println!("added root");}
        Err(e) => {println!("error adding root");*/
    builder.danger_accept_invalid_certs(true);//}
    //}
    let matches = process_options();
    if let None = matches {
        ::std::process::exit(1);
    }
    let matches = matches.unwrap();

    let message = format!("{}",matches.opt_str("m").unwrap_or("defaultmsg".to_owned()));
    let domain_str = format!("{}",matches.opt_str("c").unwrap_or("0.0.0.0".to_owned()));
    let domain: &str = &*domain_str;
    let msg = message.as_bytes();
    let (mut streamrcv, mut streamsend)  =  if matches.opt_present("c") {

        let port = format!("{}:{}",
                           matches.opt_str("c").unwrap(),
                           matches.opt_str("p").unwrap_or("12345".to_owned()));

        println!("Connecting to server {:?}...",port);
        let mut stream1 = TcpStream::connect(port).unwrap();
        let mut stream2 = stream1.try_clone().unwrap();
        (stream1,stream2)
    }else {
        println!("Need to specify adress");
        return;
    };

    streamrcv.set_nodelay(true).expect("Could not set nodelay");
    streamsend.set_nodelay(true).expect("Could not set nodelay");


    let mut seeder = rand::os::OsRng::new().unwrap();
    let mut rng = rand::ChaChaRng::new_unseeded();
    rng.set_counter(seeder.gen::<u64>(), seeder.gen::<u64>());
    let connector = builder.build();
    match connector {
        Ok(connector) => {
            //let stream = TcpStream::connect("0.0.0.0:8443").unwrap();
            let mut stream = connector.connect(domain, streamrcv).unwrap();
            let mut stream2 = connector.connect(domain, streamsend).unwrap();

            println!("connected");
            let mut buf = [0u8];
            stream.read_exact(&mut buf);
            println!("{:?}", buf);
            stream2.write("asdf".as_bytes());
            let mut buf = [0u8];
            stream.read_exact(&mut buf);
            println!("{:?}", buf);
            stream2.write("asdf".as_bytes());
            if matches.opt_present("b") {
                let skb = FSecp256Ord::from_slice(&[0xb75db4463a602ff0,
                    0x83b6a76e7fad1ec, 0xa33f33b8e9c84dbd, 0xb94fceb9fff7cfb2]);

                println!("Starting setup as Bob...");
                let bob = mpecdsa::mpecdsa::Bob2P::new(
                    &skb, &mut rng, &mut stream,&mut stream2).unwrap();
                println!("Setup finished");
                println!("The following message will be signed: {:?}", message);
                let sig=  bob.sign(
                    &msg, &mut rng, &mut stream,&mut stream2).unwrap();
                println!("Successful signature creation");
                println!("{:?}",sig);



            } else {
                let ska = FSecp256Ord::from_slice(&[0xc93d9fa738a8b4b6,
                    0xe8dd5f4af65e7462, 0xcbdf97aeca50c5c4, 0x67498f7dcab40d3]);
                println!("Starting setup as Alice...");
                let alice = mpecdsa::mpecdsa::Alice2P::new(
                    &ska, &mut rng, &mut stream, &mut stream2).unwrap();
                println!("Setup finished");
                println!("The following message will be signed: {:?}", message);
                alice.sign(&msg, &mut rng, &mut stream,&mut stream2).unwrap();
                println!("Successful signature creation");



            }

        }
        Err(e) => {println!("Connection Failed");}
    }


}